import Head from "next/head";
import { Box, Divider, Link as CLink, Heading, Text, OrderedList, ListItem, List, Button, Center } from "@chakra-ui/react";
import React from "react";
import { CardanoWallet, useWalletList, useAddress, useAssets, useLovelace, useNetwork, useWallet } from "@meshsdk/react";

export default function WalletHooks() {

  // Various wallet hooks
  const wallets = useWalletList();
  const address = useAddress();
  const assets = useAssets();
  const lovelace = useLovelace();
  const network = useNetwork();
  const { wallet, connected, name, connecting, connect, disconnect, error } = useWallet();

  return (
    <>
      <Head>
        <title>PPBL 2023 Playground</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Divider w="70%" mx="auto" pb="10" />

      <Box w={["100%", "70%"]} mx="auto" my="10">

        <Center><Heading>Mesh Wallet Hooks</Heading></Center>

        <Divider w="100%" mx="auto" pb="10" />

        <Box mx="auto" my="5" p="5" border="1px" borderRadius="md">

          <Heading size="md" pb="3">
            This page demonstrates the usage of all the wallets hooks available from the Mesh library as mentioned at 
            <CLink href="https://meshjs.dev/react/wallet-hooks"> Mesh Wallet Hooks. </CLink>
          </Heading>
          <p>
            Hooks let you use different React features from your components by allowing us to "hook" into them as per our needs.
            These features include state and lifecycle methods. You can either use built-in hooks or combine them to build your own.
            For further reading: <CLink href="https://react.dev/reference/react"> React Hooks.</CLink>
          </p>

          <br />
          <Center><CardanoWallet /></Center>
          <br />

          <OrderedList spacing={10}>

            <ListItem>
              <Heading size="md" pb="3">
                useWalletList - Returns a list of wallets installed on the user's browser.
                The list of avaliable wallets to connect to, when you hover over the Cardano Wallet component is provided by this 
                hook.
              </Heading>
              {
                wallets.map((wallet, i) => {
                  return (
                    <p key={i}>
                      <img src={wallet.icon} style={{width: '48px'}} />
                      <b>{wallet.name}</b>
                    </p>
                  );
                })
              }
            </ListItem>
            
            <ListItem>
              <Heading size="md" pb="3">useWallet - Provide information on the current wallet state and functions for 
              connecting and disconnecting the wallet.</Heading>
              <p>
                <b>Connected?: </b> {connected ? 'Is connected' : 'Not connected'}
              </p>
              <p>
                <b>Connecting wallet?: </b> {connecting ? 'Connecting...' : 'No'}
              </p>
              <p>
                <b>Name of connected wallet: </b>
                {name=="" ? "N/A" : name}
              </p>
              <br/>
              <Button onClick={() => disconnect()}>Disconnect Wallet</Button>
            </ListItem>

            <ListItem>
              <Heading size="md" pb="3">useAddress - Returns address of connected wallet.</Heading>
              {connected && address && <p>Your wallet address is: <code>{address}</code></p>}
            </ListItem>

            <ListItem>
              <Heading size="md" pb="3">useAssets - Returns a list of assets in connected wallet from all UTxOs.</Heading>
              <OrderedList>
                {connected && assets &&
                  assets.slice(0, 10).map((asset, i) => {
                    return (
                      <ListItem key={i}>
                        <b>{asset.unit}</b> (x{asset.quantity})
                      </ListItem>
                    );
                  })
                }
              </OrderedList>
            </ListItem>

            <ListItem>
              <Heading size="md" pb="3">useLovelace - Returns amount of lovelace in the wallet.</Heading>
              {connected && lovelace && <p>You have <b>₳ {parseInt(lovelace) / 1000000}</b>.</p>}
            </ListItem>

            {/* 
            Commenting out "useNetwork" because the network value is an Integer which doesn't provide any understandable information as to 
            which network is the wallet exactly on.
            <ListItem>
              <Heading size="md" pb="3">useNetwork - Returns the network which the connected wallet is running on.</Heading>
              {connected && network && <p>Connected to : <b>{network}</b></p>}
            </ListItem> */}
          </OrderedList>
          <br />

        </Box>
      </Box>
    </>
  );
}
