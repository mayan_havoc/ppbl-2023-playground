import { gql, useLazyQuery } from "@apollo/client";
import { Box, Heading, FormControl, FormLabel, Input, Button, Text, Center, Spinner } from "@chakra-ui/react";
import { useFormik } from "formik";
import { useState } from "react";
import React from "react";
import { contributorPolicyID } from "../../gpte-config";
import { stringToHex } from "../utils/index";
import { getContributorReferenceDatum } from "@/src/utils/referenceDatumHelper"

// GraphQL Query Definition:
export const UTXO_DATUM_AT_ADDRESS_WITH_REFERENCE_CONTRIBUTOR_TOKEN = gql`
query GetUTxODatumAtReferenceContributorToken($assetId: Hex!) {
    utxos(where: { tokens: { asset: { assetId: { _eq: $assetId } } } }) {
      datum {
        value
      }
    }
  }  
`;

export const QueryContributorTokenDetails = () => {

  const [alias, setAlias] = useState<string | undefined>(undefined);
  const [tokenAssetName, setTokenAssetName] = useState<string | undefined>(undefined);
  const [refTokenAssetName, setRefTokenAssetName] = useState<string | undefined>(undefined);

  const getTokenAssetName = (alias: String) => {
    return '222PPBL2023' + alias;
  }

  const getRefTokenAssetName = (alias: String) => {
    return '100PPBL2023' + alias;
  }

  const formik = useFormik({
    initialValues: {
        alias: ""
    },
    onSubmit: async (values) => {
        setAlias(values.alias);
        setTokenAssetName(getTokenAssetName(values.alias));
        setRefTokenAssetName(getRefTokenAssetName(values.alias));

        getUTxODatum({
          variables: {
            assetId: (contributorPolicyID + stringToHex(getRefTokenAssetName(formik.values.alias)))
          }
        });
    },
  });

  // useLazyQuery hook from @apollo/client library
  // Handles fetching of GraphQL data
  const [getUTxODatum, { loading, error, data }] = useLazyQuery(UTXO_DATUM_AT_ADDRESS_WITH_REFERENCE_CONTRIBUTOR_TOKEN);

  const handleClick = () => {

    setAlias(formik.values.alias);
    setTokenAssetName(getTokenAssetName(formik.values.alias));
    setRefTokenAssetName(getRefTokenAssetName(formik.values.alias));

    getUTxODatum({
      variables: {
        assetId: (contributorPolicyID + stringToHex(getRefTokenAssetName(formik.values.alias)))
      }
    });
  };

  // Boilerplate to handle useLazyQuery loading state.
  if (loading)
    return (
      <Center flexDirection="column">
        <Heading>Loading</Heading>
        <Spinner />
      </Center>
    );

  // Boilerplate to handle useLazyQuery error state.
  if (error)
    return (
      <Center>
        <Heading>Error</Heading>
        <pre>{JSON.stringify(error)}</pre>
      </Center>
    );

  // If the query is not loading or in error state, this will be rendered on the page:
  return (

    <Box bg="theme.light" color="theme.dark" p="3">
      <Heading size="md" py="3">
        Fetch PPBL2023 Token Details
      </Heading>

      <Text py="3">
        Use this form to check details of the PPBL 2023 Contributor token by providing an alias. Try your alias and any others:
      </Text>

      <FormControl p="5" bg="theme.dark" color="theme.light">
        <FormLabel>Enter a PPBL2023 Token Alias:</FormLabel>
        
        <Input
          mt="3"
          id="alias"
          name="alias"
          onChange={formik.handleChange}
          value={formik.values.alias}
        />
        
        <Button type="submit" size="sm" mt="3" color="theme.dark" onClick={handleClick}>
          Submit
        </Button>
      </FormControl>

      <br/>

      {
        data && data.utxos.length > 0 && 
        <Text>
          Your Contributor Token's Asset Name would be <b>{tokenAssetName} </b>
          & Contributor Reference Token's Asset Name would be <b>{refTokenAssetName}</b> 
          <br/>
          Your token details:<pre><b> {JSON.stringify(getContributorReferenceDatum(data.utxos[0].datum))}</b></pre>
        </Text>
      }
      
      {data && data.utxos.length == 0 && <Text>Invalid PPBL2023 token alias: <b> {alias}</b></Text>}

    </Box>
  );
};
